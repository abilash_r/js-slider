"use strict";

// replace with dynamic data;
let collections = [
    {id:1, img: "photography_00", caption: "Nikon D700"},
    {id:2, img: "photography_01", caption: "RICOHFLEX Model VI"},
    {id:3, img: "photography_02", caption: "Praktica"},
    {id:4, img: "photography_03", caption: "Vintage Classic"},
    {id:5, img: "photography_04", caption: "Alpa 10d"},
    {id:6, img: "photography_05", caption: "Reflekta II"},
];

//variable declarions
const IMG_PATH = "images/";
const MAX_SLIDE = collections.length - 1;
const MIN_SLIDE = 0;
let count = 1;
let image = document.querySelector(".gallery");
let imageTitle = document.querySelector("figcaption");
let title;
let initialImageSrc = IMG_PATH + collections[ 0 ].img + ".jpg";
let initialImageCaption = collections[ 0 ].caption;

//slider click handler
let btnCtrl = (n) => {
    count += n;
    if( count > MAX_SLIDE ) {
        count = MIN_SLIDE;
    } else if( count < MIN_SLIDE ) {
        count = MAX_SLIDE;
    }
    // console.log( `image.src = ${IMG_PATH} ${collections[count]}.jpg` );
    title = collections[ count ].caption;
    image.src = IMG_PATH + collections[ count ].img + ".jpg";
    image.alt = image.title = imageTitle.innerHTML = title;
}

//initial setup
window.onload = () => {
    image.src = initialImageSrc;
    image.alt = image.title = imageTitle.innerHTML = initialImageCaption;
}
